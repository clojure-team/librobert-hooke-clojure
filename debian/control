Source: robert-hooke
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Elana Hashman <ehashman@debian.org>
Build-Depends:
 debhelper (>= 10),
 javahelper (>= 0.32),
 maven-repo-helper (>= 1.7),
 default-jdk
Standards-Version: 4.1.3
Vcs-Git: https://salsa.debian.org/clojure-team/librobert-hooke-clojure.git
Vcs-Browser: https://salsa.debian.org/clojure-team/librobert-hooke-clojure
Homepage: https://github.com/technomancy/robert-hooke

Package: librobert-hooke-clojure
Architecture: all
Depends:
 ${java:Depends},
 ${misc:Depends},
 libclojure-java | libclojure1.8-java
Recommends: ${java:Recommends}
Description: Function wrapper library for Clojure
 Robert Hooke is a Clojure library that provides a flexible, composable
 mechanism to extend functions after they have been defined.
 .
 Hooks can be used to change the behaviour of the functions they wrap in many
 ways, for example:
 .
  * conditional execution
  * modify arguments
  * add side effects
  * return different values
 .
 Hooke is inspired by Emacs Lisp's defadvice and clojure.test fixtures.
